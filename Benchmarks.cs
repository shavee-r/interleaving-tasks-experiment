﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Threading.Tasks.Sources;
using BenchmarkDotNet;
using BenchmarkDotNet.Attributes;

namespace Experiment;

[MemoryDiagnoser]
public class Benchmarks
{
    private static readonly int[] ConnectorDelaysMs = new int[] { 300, 50, 250, 100, 200, 150 };

    // [Params(0, 2, 5, 10)]
    [Params(0, 5)]
    public int DelayMsPerResultAggregation { get; set; }

    //[Params(5, 10, 20)]
    [Params(5, 10)]
    public int NumOfConnectors { get; set; }

    private TestResultsAggregator ResultsAggregator { get; set; }  

    private TestConnector<int>[] Connectors { get; set; }

    [GlobalSetup]
    public void GlobalSetup()
    {
        ResultsAggregator = new TestResultsAggregator(DelayMsPerResultAggregation);

        Connectors = Enumerable
            .Range(0, NumOfConnectors)
            .Select(i => new TestConnector<int>(
                Response: i, 
                DelayMs: ConnectorDelaysMs[i % ConnectorDelaysMs.Length]))
            .ToArray();
    }

    [Benchmark(Baseline = true)]
    public async Task<int> ToListAndProcess()
    {
        var responseTasks = Connectors.Select(c => c.Search()).ToList();

        foreach(var responseTask in responseTasks)
        {
            ResultsAggregator.Aggregate(await responseTask);
        }

        return ResultsAggregator.Result;
    }

    [Benchmark]
    public async Task<int> WhenAllAndProcess()
    {
        var responses = await Task.WhenAll(Connectors.Select(c => c.Search()));

        foreach(var response in responses)
        {
            ResultsAggregator.Aggregate(response);
        }

        return ResultsAggregator.Result;
    }

    [Benchmark]
    public async Task<int> WhenAnyInterleave()
    {
        // See https://devblogs.microsoft.com/pfxteam/processing-tasks-as-they-complete/
        var tasks = Connectors.Select(c => c.Search()).ToList();

        while(tasks.Count > 0) {
            var t = await Task.WhenAny(tasks);
            tasks.Remove(t);

            ResultsAggregator.Aggregate(await t);
        }

        return ResultsAggregator.Result;
    }

    [Benchmark]
    public async Task<int> BetterInterleave()
    {
        // See https://devblogs.microsoft.com/pfxteam/processing-tasks-as-they-complete/
        var tasks = Connectors.Select(c => c.Search());
        
        foreach(var bucket in Interleaved(tasks)) {
            var t = await bucket;

            ResultsAggregator.Aggregate(await t);
        }

        return ResultsAggregator.Result;

        Task<Task<T>>[] Interleaved<T>(IEnumerable<Task<T>> tasks)
        {
            var inputTasks = tasks.ToList();

            var buckets = new TaskCompletionSource<Task<T>>[inputTasks.Count];
            var results = new Task<Task<T>>[buckets.Length];
            for (int i = 0; i < buckets.Length; i++) 
            {
                buckets[i] = new TaskCompletionSource<Task<T>>();
                results[i] = buckets[i].Task;
            }

            int nextTaskIndex = -1;
            Action<Task<T>> continuation = completed =>
            {
                var bucket = buckets[Interlocked.Increment(ref nextTaskIndex)];
                bucket.TrySetResult(completed);
            };

            foreach (var inputTask in inputTasks)
                inputTask.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);

            return results;
        }
    }

    [Benchmark]
    public async Task<int> BetterInterleaveV2()
    {
        var tasks = TaskExtensions.InterleaveV2(Connectors.Select(c => c.Search()));
        
        foreach(var task in tasks) {

            ResultsAggregator.Aggregate(await task);
        }

        return ResultsAggregator.Result;
    }

    [Benchmark]
    public async Task<int> ProcessDirectlyWithLock()
    {
        var semaphore = new SemaphoreSlim(1);
        var responseTasks = Connectors.Select(SearchAsync);

        await Task.WhenAll(responseTasks);

        return ResultsAggregator.Result;

        async Task SearchAsync(TestConnector<int> connector)
        {
            var response = await connector.Search();

            await semaphore.WaitAsync();
            ResultsAggregator.Aggregate(response);
            semaphore.Release();
        }
    }

    [Benchmark]
    public async Task<int> ToAsyncEnumerable()
    {
        var responses = TaskExtensions.Interleave(Connectors.Select(c => c.Search()));

        await foreach(var response in responses)
        {
            ResultsAggregator.Aggregate(response);
        }

        return ResultsAggregator.Result;
    }
}

record TestConnector<TRes>(TRes Response, int DelayMs)
{
    public async Task<TRes> Search() 
    {
        await Task.Delay(DelayMs);
        
        return Response;
    }
}

record TestResultsAggregator(int DelayMsPerAggregation = 0)
{
    public int Result { get; private set; } = 0;
    private Stopwatch sw = new Stopwatch();

    public void Aggregate(int value)
    {
        if (DelayMsPerAggregation > 0)
        {
            sw.Start();
            while (sw.ElapsedMilliseconds < DelayMsPerAggregation) {}
            sw.Reset();
        }

        Result += value;
    }
}

static class TaskExtensions
{
    public static IAsyncEnumerable<T> Interleave<T>(IEnumerable<Task<T>> tasks)
    {
        var channel = Channel.CreateUnbounded<T>(options: new UnboundedChannelOptions()
        {
            SingleReader = true, 
        });

        var numOfTasks = 0;
        foreach(var task in tasks)
        {
            var _ = ExecuteAndWriteAsync(task, channel.Writer);
            numOfTasks += 1;
        }

        return ReadAllAsync(channel.Reader, numOfTasks);

        async Task ExecuteAndWriteAsync(Task<T> task, ChannelWriter<T> writer)
        {
            var result = await task;
            await writer.WriteAsync(result);
        }

        async IAsyncEnumerable<T> ReadAllAsync(ChannelReader<T> reader, int numOfItems)
        {
            while (numOfItems > 0)
            {
                await reader.WaitToReadAsync();
                while (reader.TryRead(out var item))
                {
                    yield return item;
                    numOfItems -= 1;
                }
            }
        }
    }
    public static List<Task<T>> InterleaveV2<T>(IEnumerable<Task<T>> tasks)
    {
        var buckets = new List<TaskCompletionSource<T>>();
        var results = new List<Task<T>>();

        int nextTaskIndex = -1;
        foreach (var task in tasks)
        {
            var bucket = new TaskCompletionSource<T>();

            buckets.Add(bucket);
            results.Add(bucket.Task);
            
            var _ = ExecuteAndWriteAsync(task);
        }

        return results;

        async Task ExecuteAndWriteAsync(Task<T> task)
        {
            var result = await task;
            var bucket = buckets[Interlocked.Increment(ref nextTaskIndex)];
            bucket.TrySetResult(result);
        }
    }
}